import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import errorReducer, { ErrorAction } from "./reducers/errorReducer";
import localizeReducer, { LocalizeAction } from "./reducers/localizeReducer";

const rootReducers = combineReducers({

  error: errorReducer,
  localize: localizeReducer
});

export type StoreType = ReturnType<typeof rootReducers>;

const store = createStore(rootReducers, applyMiddleware(thunk));
export default store;

// need to extend all action type here
type ActionBaseType = ErrorAction | LocalizeAction;

export interface IBaseAction {
  type: ActionBaseType;
}
