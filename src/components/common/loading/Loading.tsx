import "./Loading.scss";
import React, { useState, useImperativeHandle } from "react";
import { Backdrop, CircularProgress } from "@material-ui/core";

const __RootLoadingRef = React.createRef<{ show: () => void; hide: () => void }>();

export interface LoadingRootProps {}

const LoadingRoot: React.FC<LoadingRootProps> = (props) => {
  const [show, setShow] = useState(false);

  useImperativeHandle(__RootLoadingRef, () => ({
    show: () => setTimeout(() => setShow(true), 0),
    hide: () => setTimeout(() => setShow(false), 0),
  }));

  return (
    <Backdrop className="com-loading" open={show}>
      <CircularProgress color="secondary" />
    </Backdrop>
  );
};

export default LoadingRoot;

// function use

export const showLoading = (time?: number) => {
  __RootLoadingRef.current?.show();
  if (time && time > 0 && __RootLoadingRef.current?.hide) {
    setTimeout(() => {
      hideLoading();
    }, time * 1000);
  }
};

export const hideLoading = () => {
  __RootLoadingRef.current?.hide();
};
