import "./SuccessSignup.scss";
import React from "react";

const SuccessSignup: React.FC = () => {
 
  return (
    <div className="com-success-signup">
      <div className="container">
        <div className="text-container">
          <div className="title">SUCCESS!</div>
          <div className="description">
            You've signed up successfully! Please see your registration <a href="/subscribers">HERE</a>
          </div>
        </div>
      </div>
    </div>
        
  );
};

export default SuccessSignup;
