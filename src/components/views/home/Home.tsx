import "./Home.scss";
import React, {useState } from "react";
import { useForm } from "react-hook-form";
import { FormContext } from "react-hook-form";
import api from "api";
import { showLoading, hideLoading } from "components/common/loading/Loading";
import ReCAPTCHA from "react-google-recaptcha";
import SuccessSignup from "../success-signup/SuccessSignup";


interface FormDataProps {
  firstname: string,
  lastname: string,
  email: string
};

const Home: React.FC = () => {
  const formMethods = useForm();
  const { register, handleSubmit, errors } = useForm<FormDataProps>();
  const [selectedFile, setSelectedFile] = useState<any|null>(null);
  const recaptchaRef:any = React.useRef();
  const [isRobot, setIsRobot] = useState<boolean|false>();
  const [isSuccessSignedUp, setIsSuccessSignedUp] = useState<boolean|false>()
 
  const onSubmit = async (data:FormDataProps|undefined) => {    
    showLoading();
    setIsRobot(false);
    if (data !== undefined && recaptchaRef !== undefined) {
      const captcha = await recaptchaRef.current.executeAsync();
      if (captcha !== null && captcha !== undefined) {
        const formData = new FormData();
        
        formData.append('firstname', data.firstname);
        formData.append('lastname', data.lastname);
        formData.append('email', data.email);
        formData.append('captcha', captcha);
        if (selectedFile !== null && selectedFile !== undefined) {
          formData.append('file', selectedFile);
        }

        const res = await api.post('/subscription','sub', formData, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        });

        if (res.status === 200) {
          setIsSuccessSignedUp(true);
        }else {
          setIsSuccessSignedUp(false);
        }
    }
    else {
      setIsRobot(true);
    }
    
    hideLoading();
  }
  };

  const onFileChange = (e:any) => {
    setSelectedFile(e.target.files[0]);
  }

  return (
    <div className="com-home">
      {isSuccessSignedUp && <SuccessSignup>a</SuccessSignup>}
      {!isSuccessSignedUp &&
      <div className="container">
        <div className="text-container">
          <div className="title">Signup for the Galactic Empire!</div>
        </div>

        <div className="form-container">
          <FormContext {...formMethods}>
          <form id="form_signup"  onSubmit={handleSubmit(onSubmit)} encType="multipart/form-data">
            <label>First Name</label>
            <input type="text" name="firstname" placeholder="First Name" ref={register({ required: true })} />
            {errors.firstname && <span className="error">First Name is required</span>}

            <label>Last Name</label>
            <input type="text" name="lastname" placeholder="Last Name" ref={register({ required: true })} />
            {errors.firstname && <span className="error">Last Name is required</span>}

            <label>Email Name</label>
            <input type="text" name="email" placeholder="Email" ref={register({ required: true, pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i })} />
            {errors.email && errors.email.type === "required" && <span className="error">Email is required</span>}
            {errors.email && errors.email.type === "pattern" && <span className="error">Invalid email</span>}

            <label>Portrait photo </label>
            <input type="file" name="file" onChange={onFileChange}/>

            {isRobot && <span className="error">You are not passed recaptcha!</span>}
    
            <button className="signup">Signup</button>
            <ReCAPTCHA
              ref={recaptchaRef}
              size="invisible"
              sitekey="6LdWMQ8aAAAAAOs44FNSEpxd8Ekf6UOYamc_5apc"
            />
          </form>
          </FormContext>
        </div>
      </div>}
    </div>
  );
};

export default Home;
