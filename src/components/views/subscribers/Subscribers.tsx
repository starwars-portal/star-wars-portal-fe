import "./Subscribers.scss";
import React, { useEffect, useState } from "react";

import api from "api";

const Subscribers: React.FC = () => {
  const [subscribers, setSubscribers] = useState<any[]>();
   // check for login
   useEffect( () => {
    api.get("/subscription/list", "get subscription").then(response =>{
      let data = [];
      if (response.status === 200) {
        data = response.data;
        setSubscribers(data);
      }
    });
  }, []);
  
  return (
    <div className="com-subscribers">
      <div className="container">
      <div className="text-container">
          <div className="title">The Galactic Empire members</div>
        </div>
      <div className="subscriber-list">
        <table>
          <thead>
          <tr className="com-subscriber">
            <td className="id">#</td>
            <td className="photo">Photo</td>
            <td className="firstname" >First Name</td>
            <td className="lastname">Last Name</td>
          </tr>
          </thead>
          <tbody>
            {subscribers &&
                  subscribers?.map((subscriber) => (
              <tr className="com-subscriber" key="{subscriber.id}">
                <td className="id">{subscriber.id}</td>
                <td className="photo"><img src={subscriber.profile_image} alt="Star wars portal" /></td>
                <td className="firstname" >{subscriber.firstname}</td>
                <td className="lastname">{subscriber.lastname}</td>
              </tr>
                  ))
              }
              </tbody>
              </table>
      </div>
    </div>
    </div>
  );
};

export default Subscribers;
