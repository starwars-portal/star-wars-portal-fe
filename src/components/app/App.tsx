import React from "react";
import "./App.scss";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useLanguage } from "store/actions/localizeAction";

import { ScrollToTop } from "libs/utils";
import Home from "../views/home/Home";
import Subscribers from "../views/subscribers/Subscribers";

const App: React.FC = () => {
  useLanguage(); // load save/default language
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div className="com-app">
        <ScrollToTop />
        <Switch>
          <Route path="/subscribers/">
            <Subscribers></Subscribers>
          </Route>

          <Route path="/" exact>
            <Home></Home>
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
